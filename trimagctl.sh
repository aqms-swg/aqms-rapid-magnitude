#!/bin/bash

# init startup/shutdown/restart/status action script 
PROG='trimag'
CFG_TYPE_TAGS='fast slow'
CFGDIR="$AQMS_HOME/run/cfg/mag"
LOGDIR="$AQMS_HOME/run/logs"

if [ -z "$RT_HOME" -o -z "$LD_LIBRARY_PATH" ]; then source /usr/local/etc/aqms.env; fi

home="$AQMS_HOME/bin"
cfg="$AQMS_HOME/run/cfg/mag"
ctlcfgfile="$CFGDIR/${PROG}ctl.cfg"
cd $home

# Displays a usage message and exits.
function usage() {
  echo "Script action is to start, stop, restart, or check status of $PROG processes."
  echo "Usage: $(basename $0) <action: start|stop|restart|status|help> [type]"
  echo "If optional type tag is not specified on the command line, the script processes"
  echo "all type tags specified in the file: $ctlcfgfile. The type tags are space"
  echo "delimited, and can be on one or more lines, where a comment line begin with a #."
  echo "The usual $PROG type tags are: $CFG_TYPE_TAGS. The $PROG binary is run with a "
  echo "configuration file argument whose name is constructed by using the type"
  echo "tag in a filename template of the form: ${CFGDIR}/${PROG}_<type>.cfg"
  exit $1 
}

# Gets the PID of a running cfg type.
function get_pid {
  local ctype=$1
  if [ -n "$ctype" ] 
  then
    echo `pgrep -f "${PROG}.*${CFGDIR}/${PROG}_${ctype}.cfg"`
  fi 
}

# Constructs the command to start a cfg type.
function get_cmd {
  local ctype=$1
  if [ -n "$ctype" ] 
  then
    local cfgfile=${CFGDIR}/${PROG}_${ctype}.cfg
    if [ ! -f "$cfgfile" ]; then
        echo "Error: Missing cfg file, type $ctype filename: $cfgfile" >&2
        return
    fi
    echo "./${PROG} ${CFGDIR}/${PROG}_${ctype}.cfg"
  fi
}

# Gets the conlog command for a type.
function get_conlog {
  local ctype=$1
  if [ -n "$ctype" ] 
  then
    echo "conlog ${LOGDIR}/${PROG}_${ctype}_console"
  fi
}

# Starts a process of cfg  type.
function start_type {
  local ctype=$1
  
  timestamp=`date -u +'%F %T %Z'`

  if [ -n "$ctype" ] 
  then
    pid=$(get_pid $ctype)
    if [ "$pid" != "" ]
    then
      echo "$PROG ${ctype} is already running with pid $pid @ $timestamp."
    else
      echo "Starting $PROG ${ctype} @ $timestamp ..."
      # Construct the $PROG and conlog commands.
      local cmd="$(get_cmd $ctype)" 
      if [ -z "$cmd" ]; then exit 1; fi
      local conlog="$(get_conlog $ctype)"
      #echo "$cmd 2>&1 | $conlog &"
      $cmd 2>&1 | $conlog &
      sleep 1
      timestamp=`date -u +'%F %T %Z'`
      echo "Started $PROG ${ctype} with pid $(get_pid ${ctype}) @ $timestamp"
    fi
  fi
}

# Kills a program cfg type.
function stop_type {
  local ctype=$1
  local pid=$(get_pid $ctype)
  timestamp=`date -u +'%F %T %Z'`
  if [ "$pid" == "" ]
  then
    echo "${PROG} ${ctype} is not running @ $timestamp."
  else
    echo "Killing $PROG ${ctype} pid $pid @ $timestamp"
    kill $pid
    sleep 1
    pid=$(get_pid $ctype)
    if [ -n "$pid" ]; then 
        kill -9 "$pid"
    fi
  fi
}

# Calls start_type to start process of specified cfg types
function start {
  if [ "$TYPE" == "" ]
  then
    if [ -f "$ctlcfgfile" ]; then
      while read line; do
        if [[ "$line" =~ ^#.*$ ]]; then # skip comment
            continue
        fi
        for x in $line; do
            start_type $x
        done
      done < $ctlcfgfile 
    else
      usage 1
    fi
  else
    start_type $TYPE
  fi

}

# Calls stop_type to stop process of specified cfg types.
function stop {
  if [ "$TYPE" == "" ]
  then
    if [ -f "$ctlcfgfile" ]; then
      while read line; do
        if [[ "$line" =~ ^#.*$ ]]; then # skip comment
            continue
        fi
        for x in $line; do
            stop_type $x
        done
      done < $ctlcfgfile 
    else
      usage 1
    fi
  else
     stop_type $TYPE
  fi
}

function print_status {
  timestamp=`date -u +'%F %T %Z'`
  local prog=$1
  local ctype="$2"
  local pids="$3"
  if [ -n "$pids"  ]; then
   echo "$prog $ctype processes running @ $timestamp"
   echo "$pids"
  else
    echo "No $prog $ctype processes found @ $timestamp"
  fi
}

###############################################################################################
function get_status {
  local pids=''
  if [ "$TYPE" == "" ]; then
    if [ -f "$ctlcfgfile" ]; then
      while read line; do
        if [[ "$line" =~ ^#.*$ ]]; then # skip comment
            continue
        fi
        for x in $line; do
            pids=`ps -ef | egrep -e "${PROG}_${x}.*\.cfg|${PROG}_${x}_console" | grep -v 'egrep -e' | sort -k 9`
            print_status ${PROG} $x "$pids"
        done
      done < $ctlcfgfile 
    fi
  else
    pids=`ps -ef | egrep -e "${PROG}_${TYPE}.*\.cfg|${PROG}_${TYPE}_console" | grep -v 'egrep -e' | sort -k 9`
    print_status ${PROG} $TYPE "$pids"
  fi
}
###############################################################################################

ACTION=$1
TYPE=$2

if [ "$ACTION" == "--help" -o "$ACTION" == "-h" ]
then
  usage 0
fi

if [ -z "$TYPE" -a ! -f "$ctlcfgfile" ]; then
  echo "Error: Missing the required cfg file: $ctlcfgfile for default $PROG process types to start/stop."
  echo "       Otherwise you must specify optional command line cfg file type tag (e.g. $CFG_TYPE_TAGS)."
  echo
  usage 1
fi

case "$ACTION" in
'start')
  start $TYPE
  ;;

'stop')
  stop $TYPE
  ;;

'restart')
  stop $TYPE
  sleep 2
  start $TYPE
  ;;

'status')
  get_status $TYPE
  ;;

'help')
  usage 0
  ;;
*)
  echo "Unknown action: $ACTION"
  usage 2
  ;;

esac
 
exit
