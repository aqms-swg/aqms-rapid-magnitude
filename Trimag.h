/** @file
 * @ingroup group_rapid_magnitude
 * @brief Header file for Trimag.C
 */
/***********************************************************/
/*
File Name :
        Trimag.h

Original Author:
        Patrick Small

Description:


Creation Date:
        7 June 2000

Modification History:
	23 Dec 2007 - leap second changes

   25 Aug 2014 AWW - Added double nextEventSecs class property member, its
                      value sets like member in DatabaseTrimag class for use
                      in _GetNextOrigin call.

   14 Jun 2016 AWW - Added double precedingEventSecs class property member
                      used in TrimagDatabase HasPrecedingEvent call.
                      Added double maxPrelimFinalMlDiff class property member, its
                      value sets like member in the MagCalc class for use in determing
                      whether to use final or prelim ml when another event precedes.

	09 Dec 2016 - 64-bit system compliance updates performed
*/
/**********************************************************/

#ifndef trimag_H
#define trimag_H

// Various include files
#include <map>
#include <stdint.h>
#include "gcda.h"
#include "dreader.h"
#include "dwriter.h"
#include "dcmon.h"
#include "ddcr.h"
#include "AmplitudeADA.h"
#include "TimeStamp.h"
#include "RTApplication.h"
#include "Event.h"
#include "EventArchiver.h"
#include "MagCalc.h"
#include "LockFile.h"
#include "Tlay.h"
#include "ewLogA0.h"


/// Definition of an Amplitude reader
typedef Defined_Data_Channel_Reader<struct amplitude_data_type>  AmpReader;

/// Definition of a Channel-Amp Reader mapping
typedef std::map<Channel, AmpReader, std::less<Channel>, 
	std::allocator<std::pair<const Channel, AmpReader> > > ChanReaderMap;

/// Definition of an Event list
typedef std::multimap<TimeStamp, Event, std::less<TimeStamp>, 
	std::allocator<std::pair<const TimeStamp, Event> > > EventList;

/// Class to setup framework for magnitude calculation derived from aqms-libs/tnframework
class Trimag : public RTApplication 
{
 private:
    char auth[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    char adafile[MAXSTR];
    char final_ml_magalgo[DB_MAX_ALGORITHM_LEN+1];
    char prelim_ml_magalgo[DB_MAX_ALGORITHM_LEN+1];
    char me_magalgo[DB_MAX_ALGORITHM_LEN+1];

    char ada_key_name[GCDA_KEY_NAME_LEN];

    char velocity_model_file[MAXSTR];
    char ewloga0_file[MAXSTR];
    char archive_directory[MAXSTR];
    char lock_file_name[MAXSTR];
    LockFile *lock;

    key_t ada_key; 	// needed for amp reader creation now 

    int propdelay;
    int datalatency;
    double precedingEventSecs;
    double maxPrelimFinalMlDiff;
    double nextEventSecs;
    double maxOrg2OrgDist;
    double outlierthres;
    double minFinalMlCutoff;
    double mincutoff;
    double maxcutoff;
    double stepcutoff;
    double defaultcutoff;
    double mlCutLineSlope;
    double mlCutLineIntercept;
    int minchannels;
    int min_ml_channels;
    int minstations;
    double minhh;
    double maxhh;
    double minhl;
    double maxhl;
    double snr_cutoff;
    double mlclipvalue;
    double postSMult;
    int minmechans;
    double maxmerms;
    double minMdMag;
    int tryMlOnNoMd;
    int prebuf;
    int postbuf;
    int only_use_horizontal;
    int require_both_horizontals;
    int always_publish_completion;
    int use_CIT_windows;
    int writeMagnitudes;
    int verboseFindPeaks;

    int Mladjust; // set to 1 if it will be used.
    double minMladjust, maxMladjust; // Thresholds for adjusting the Ml value to closer match Mw
    double aValueMladjust, bValueMladjust;  // ML_adjusted  = aValueMladjust + bValueMladjust *  Ml

    int limit_amps_num;
    double limit_amps_cutoff;
    double prelimWinTd;
    double finalWinTd;

    char eventsubject[MAXSTR];
    char magsubject[MAXSTR];
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;

    EventList events;
    EventArchiver old_events;
    Generic_CDA<struct amplitude_data_type> *ada;
    ChanReaderMap chans;
    Tlay tlay;
    ewLogA0 *ewloga0;

    int _Cleanup();
    int _LoadChannels();
    int _SendEventSignal(uint32_t evid);
    int _GetAmplitudes(Origin &org, ChanSampleList &amps);
    int _FindMag(Event &e);
    int _WriteMagnitudes(Event &e);

 public:

    Trimag(); /**< Constructor */
    ~Trimag(); /**< Destructor */
    int ParseConfiguration(const char *tag, const char *value); /**< Parse config file */
    int Startup(); /**< Initialize trimag */
    int Run(); /**< Run trimag */
    int Shutdown(); /**< Clean up and shutdown trimag */
    int DoEvent(uint32_t evid); /**< Process an event for magnitude calculation */
};

#endif
