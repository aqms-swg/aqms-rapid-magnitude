########################################################################
#
# Makefile     : trimag
#
# Author       : Paul Friberg
#
# Last Revised : November 11, 2003
# History      : 
#
#
########################################################################

include $(DEVROOT)/shared/makefiles/Make.includes
BIN	= trimag 

# uncomment this next line for enabling verbose debug messages to the log
#DEBUG_FLAGS=-DDEBUG_GCDA -DDEBUG_CORR -DDEBUG_OTL -DDEBUG_OTL2
#DEBUG_FLAGS=-DDEBUG_OTL -DDEBUG_OTL2
#DEBUG_FLAGS= -DDEBUG_PRELIM_MLWIN -DDEBUG_FINAL_MLWIN -DDEBUG_MEWIN
#DEBUG_FLAGS= -DDEBUG_MISSING_ME_ADA
#DEBUG_FLAGS= -DDEBUG_FINAL_MLWIN -DDEBUG_PRELIM_MLWIN 

INCL	= $(RTSTDINCL) $(EWINCL) -I$(DEVROOT)/lib/ewlib/include

LIBS	= $(RTSTDLIBS) -lgcda -lgfortran -lpthread


# if you just want the ew objs needed for this
#EWOBJS = $(EWLIB)/tlay.o $(EWLIB)/brent.o \
#         $(EWLIB)/kom.o $(EWLIB)/mnbrak.o


OBJS =  DatabaseTrimag.o Main.o EventSignalHandler.o \
	Peak.o MagCalc.o Samples.o Trimag.o

TOBJS = TlayTest.o

MLOBJS = getMl.o

########################################################################

all:$(BIN)

trimag: $(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(EWOBJS) $(LIBS)  -lew

tlay_test: $(TOBJS)
	$(CC) $(CFLAGS) $(TOBJS) -o $@ $(LIBS) -lew $(EWOBJS)  -ltnstd

getMl: $(MLOBJS)
	$(CC) $(CFLAGS) $(MLOBJS) -o $@ $(LIBS)

.C.o: 
	$(CC) $(CFLAGS) $(DEBUG_FLAGS) $(INCL)  $< -c

clean:
	-rm -f *.o *~ core $(BIN)
	-rm -f -r ./SunWS_cache
