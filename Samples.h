/** @file                                                                                          
 * @ingroup group_rapid_magnitude                                                                  
 * @brief Header file for Samples.C
 */
/***********************************************************

File Name :
        Samples.h

Original Author:
        Patrick Small

Description:


Creation Date:
        07 June 2000


Modification History:
	23 Dec 2007 - leap second changes


Usage Notes:


**********************************************************/

#ifndef samples_H
#define samples_H

// Various include files
#include <map>
#include "TimeStamp.h"
#include "Duration.h"
#include "AmplitudeADA.h"

// Definition of an Amplitude Data list
typedef std::map<TimeStamp, amplitude_data_type, std::less<TimeStamp>, 
    	std::allocator<std::pair<const TimeStamp, amplitude_data_type> > > SampleList;


class Samples
{
 private:

 public:
    SampleList samples;
    int validcorr;
    int valid_mlcorr;
    int valid_mecorr;
    float mlcorr_used;
    float mecorr_used;
    int instrument_type;	 // BROADBAND or STRONG_MOTION

    Samples();
    Samples(const Samples &s);
    ~Samples();

    Samples& operator=(const Samples &s);
};

#endif
