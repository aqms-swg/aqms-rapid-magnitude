/** @file
 * @ingroup group_rapid_magnitude
 * @brief Header file for MagCalc.C
 */
/***********************************************************/
/*
File Name :
        MagCalc.h

Original Author:
        Patrick Small

Description:


Creation Date:
        19 June 2000


Modification History:
	23 Dec 2007 - leap second changes
    25 Aug 2014 - Added a Origin org2 input arg to the private member function arg lists.
                  Added property members: double prelimWinTd and double finalWinTd
                  and new functions to set their values.

    14 Jun 2016  AWW - added hasPrecedingEvent int member to indicate another event is inside some leading window seconds before the current event's origin

    14 Jun 2016  AWW - added maxPrelimFinalMlDiff double member when another event is inside some leading window seconds before the current event's origin
                       set best mag to prelmin when diff exceeds this threshold

Usage Notes:
*/
/**********************************************************/

#ifndef magcalc_H
#define magcalc_H

// Various include files
#include <map>
#include <set>
#include "AmplitudeADA.h"
#include "DatabaseLimits.h"
#include "RTStatusManager.h"
#include "Event.h"
#include "Peak.h"
#include "TimeStamp.h"
#include "TimeWindow.h"
#include "Station.h"
#include "SeismoFuncs.h"
#include "Samples.h"
#include "Tlay.h"
#include "ewLogA0.h"


/// Definition of a Channel - Amplitude Data list
typedef std::map<Channel, Samples, std::less<Channel>, 
		std::allocator<std::pair<const Channel, Samples> > > ChanSampleList;

/// Definition of a sorted Magnitude list
typedef std::multiset<double, std::less<double>, std::allocator<double> > SortedMagList;

/// Definition of a station list
typedef std::set<Station, std::less<Station>, std::allocator<Station> > StationList;

/*! 
Class for rapid magnitude calculation
*/
class MagCalc
{
 private:
    RTStatusManager sm;
    Tlay tl;
    char prelim_ml_magalgo[DB_MAX_ALGORITHM_LEN+1];
    char final_ml_magalgo[DB_MAX_ALGORITHM_LEN+1];
    char me_magalgo[DB_MAX_ALGORITHM_LEN+1];
    double minFinalMlCutoff;
    double mincutoff;
    double maxcutoff;
    double stepcutoff;
    double defaultcutoff;
    double outlierthres;
    int minchannels;
    int min_ml_channels;
    int minstations;
    int require_both_horizontals;
    double min_bb;
    double max_bb;
    double min_sm;
    double max_sm;
    double mlclipvalue;
    int minmechans;
    int only_use_horizontal_for_ml;	// true false flag
    int use_CIT_windows;
    int hasPrecedingEvent;
    double maxPrelimFinalMlDiff;
    double maxmerms;
    double prebuf;
    double postbuf;
    double postSMult;
    double prelimWinTd;
    double finalWinTd;
    double snr_min_cutoff;
    double max_org2org_dist;
    double mlCutLineSlope;
    double mlCutLineIntercept;
    int verboseFindPeaks;

    int limit_amps_num;
    double limit_amps_dist_cutoff;

    double minMladjust, maxMladjust, aValueMladjust, bValueMladjust;
    ewLogA0  *ewloga0;

    int _GetCutoffDist(int valid, Magnitude &mag, double &cutoff);
    int _GetCutoffDistMe(int valid, Magnitude &mag, double &cutoff);

    int _GetPrelimInfo(Origin &org, Origin &org2, const Channel &chan, TimeWindow &win, double &dist);
    int _GetFinalInfoMl(Origin &org, Origin &org2, const Channel &chan, Magnitude &premagml, int validpremag, TimeWindow &win, double &dist);
    int _GetFinalInfoMe(Origin &org, Origin &org2, const Channel &chan, Magnitude &bestmagml, int validbestmag, TimeWindow &win, double &dist);
    int _filterPeakList(PeakList &cutpeaks, double &cutrms);
    int _RemoveOutliers(PeakList &peaks, double &rms);
    int _RemoveLoneHorizontals(PeakList &peaks);
    void _GetStationList(PeakList &peaks, StationList &sl);
    double _GetMaxGap(StationList &sl, Origin &org);
    int _IsInWindow(TimeStamp t, TimeWindow win);
    int _IsInMlWindow(TimeStamp t, TimeWindow win);
    ampScale _GetScale(const Channel &chan, int scaleval);
    int _FindPreliminaryPeaks(Origin &org, Origin &org2, ChanSampleList &amps, PeakList &prelimpeaks);
    int _ConvertDataPoints(double depth, PeakList &peaks, seismoMagType magtype);

    int _FindFinalPeaksMl(Origin &org, Origin &org2, ChanSampleList &amps, Magnitude &premagml, int validpremag, PeakList &finalmlpeaks);
    int _FindFinalPeaksMe(Origin &org, Origin &org2, ChanSampleList &amps, Magnitude &bestmagml, int validbestmag, PeakList &finalmepeaks);

    int _FindPrelimMagMl(Event &e, Origin &org, PeakList &mlpeaks, Magnitude &premagml);
    int _FindFinalMagMl(Event &e, Origin &org, Magnitude &premagml, int validpremag, PeakList &mlpeaks, Magnitude &finalmagml);
    int _FindFinalMagMe(Event &e, Origin &org, Magnitude &premagml, int validpremag, PeakList &mepeaks, Magnitude &finalmagme); 
    int _GetPrelimMagMl(Event &e, Origin &org, Origin &org2, ChanSampleList &amps, Magnitude &premagml);
    int _GetFinalMagMl(Event &e, Origin &org, Origin &org2, ChanSampleList &amps, Magnitude &premagml, int validpremag, Magnitude &inalmagml);
    int _GetFinalMagMe(Event &e, Origin &org, Origin &org2, ChanSampleList &amps, Magnitude &premagml, int validpremag, Magnitude &finalmagme);

 public:

    MagCalc(const RTStatusManager &statman, Tlay &tlay);
    MagCalc(const MagCalc &m);
    ~MagCalc();

    int SetMlCutoffLineSlope(double slope);
    int SetMlCutoffLineIntercept(double intercept);
    int SetCutoff(double mincut, double minFinalCut, double maxcut, double stepcut, double defaultcut);
    int SetSNRCutoff(double snr);
    int SetOnlyUseHorizontalForMl(int flag);
    int SetRequireBothHorizontals(int flag);
    int SetOutlierThreshold(double thres);
    int SetMinMlChannels(int num);
    int SetMinChannels(int num);
    int SetMinStations(int num);
    int SetMaxOrg2OrgDist(double num);
    int SetBBRange(double minval, double maxval);
    int SetSMRange(double minval, double maxval);
    int SetCriteriaMe(double mlclip, int minchan, double rms);
    int SetPrePBuffer(double buf);
    int SetPrelimWinTd(double td);
    int SetFinalWinTd(double td);
    int SetPostBuffer(double buf);
    int SetPostSMult(double buf);
    int SetCITwindows(int ucw);
    int SetPrelimMagalgo(char * ma);
    int SetFinalMagalgo(char * ma);
    int SetMeMagalgo(char * ma);
    int SetEWLogA0(ewLogA0 *);
    int SetMlAdjust( double a, double b, double min, double max);
    int SetLimitAmps( double cutoff, int num);
    int SetVerboseFindPeaks(int flag);
    int SetMaxPrelimFinalMlDiff(double diff);
    int SetHasPrecedingEvent(int tf);

    int GetMagnitudes(ChanSampleList &amps, Event &e);

    MagCalc& operator=(const MagCalc &m);
};

#endif
