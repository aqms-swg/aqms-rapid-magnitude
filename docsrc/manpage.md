# trimag {#man-trimag}

Rapid ML and ME magnitude generator

## PAGE LAST UPDATED ON

2020-02-25

## NAME

Trimag - Rapid ML and ME magnitude generator

## VERSION and STATUS

Version v0.2.17 2018-08-16  
status : ACTIVE
   
## PURPOSE

trimag is a module for computing Me (Energy) and Ml (Richter) magnitudes. It uses the Amplitude Data Area (ADA) for pulling me100 and ml100 values and computing the magnitudes. The long description of this process is below.

At startup, Trimag reads in a list of channels and parameters to be used in the magnitude calculations from the database. It reads the channels list from the Oracle database. It then reads Me and Ml station correction values used for magnitude calculations from the stacorrections table. The parameters used for the Trimag magnitude calculations are read in from a channel_data and simple_response tables.

Trimag then sleeps and waits to receive a signal, using the CMS software, that indicates an event has been created and loaded into the Oracle database. It detects a specific event labeled as a TN_TMT_EVENT_SIG that comes from the event coordinator program (ec).

Once trimag has been signaled to calculate magnitudes, it contacts the Oracle Database specified in its start up configuration and gets the hypocenter information from the event and origin tables. The program creates a time window for each configured channel, starting at the origin time and lasting for the propagation delay (configurable, see below) plus a data latency time (also configurable below). This time window is just used to extract ADA's for each channel. The program then waits for a specified amount of time (propagation delay + maximum data latency: both are configurable values in the trimag.cfg file) after the origin time before going to retrieve the ADA's (to assure they have all arrived). It then requests the amplitude readings for each time window from the appropriate channel ADA's. The ADA's retrieved should be sufficient to bracket any Ml100 or Me100 peaks for the network regardless of the event's distance.

Trimag calculates a preliminary Ml magnitude. The preliminary Ml magnitude is used to calculate a cutoff distance for the final computation of Ml and Me values and to compute the Ml and Me peak search windows. In addition, the preliminary Ml will be used as the reported Ml value if the "final" Ml computation fails all of the statistical test criteria.

Trimag then proceeds to calculate two magnitudes (Ml and Me). Both magnitudes require a peak amplitude to be gleaned from the amplitude window of each channel. However, each algorithm scans different portions of the time window to find this peak. It is possible for Trimag to be unable to find an Ml or an Me (or both) for any particular event. Each magnitude calculation applies different filters to the amplitude window to find the peak, and the data may be so incomplete that the program lacks the information it needs to compute a valid magnitude. The exact details of the Ml and Me calculations are provided in the sections below, but the Final Ml is a median computation over station magnitudes and the Me is an average of the station magnitudes.

At the end of processing, the Ml and/or Me magnitudes are written to the database and associated with the event. The on-scale amplitudes used in each magnitude calculation are saved and associated with their respective magnitudes as well. When Trimag finishes processing an event, it sends a signal via CMS to indicate that a magnitude has been calculated. Downstream programs pick up the signal and act upon it for alarming and trigger coordination.


## HOW TO RUN
```
usage: ./trimag <config file> 
```
## CONFIGURATION FILE

The config file is named trimag.cfg and has the following parameters. Format is

```
<parameter name> <data type> <default value>  
<description>
```

**General configuration parameters**  

**TSSOption** OR CMSConfig *string*  
CMS config file with path prefix.

**Logfile** *string*  
Path with a log file prefix where daily logs will be stored.

**Lockfile** *string*  
Path to a .lock file. If you only wish one instance of this module to run, then the lockfile name should be unique per instance.

**ConsoleLog** *number*  
This is a boolean, set to 1 to log console messages to log file, set to 0 to not do so  

**LoggingLevel** *number*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  

**ProgramName** *string*  
The unique name to allow this module to connect to CMS and be identifed.  


**HSInterval** *number*  
Seconds to wait before sending the next heartbeat signal.    

**Include** *string*  
Config file with path prefix to be included into this config.
  

**GCDA configuration parameters**  

**ADAKeyName** *string*  
This is the key name to look up in the GCDA config file pointed to by the GCDA_FILE env variable.  
  
  
**Magnitude computation start parameters**  
  
**PropagationDelay** *number*  
PropagationDelay is estimated time, in seconds, for P wave to propagate across the entire network. This parameter governs the time-window for which ADA's are retrieved for each channel and in addition to the MaxDataLatency (see below) governs the time the trimag program waits before going to get data from the ADAs. Time window requested for ADA peak amps is from origin_time to origin_time + 80 + PropagationDelay, where 80 is the maximum time duration, in seconds (after S wave arrival) that could be needed for magnitude calculations. Set this parameter to value *n* for fast computation and value *N* > *n* for slow computation. 
  
**MaxDataLatency** *number*  
This is the estimated maximum time (in seconds) for data to be transmitted from the data logger to the data acquisition computers. This is only used with the PropagationDelay to govern how long trimag should wait before retrieving ADA's. Earliest time to start processing an event = origin_time_epoch_secs + PropagationDelay + MaxDataLatency. Set this parameter to value *n* for fast computation and value *N* > *n* for slow computation.  

**Preliminary Ml computation**  
  
*Prelminary Ml Cutoff Distance Calculation* :  Once all the windows are checked for peak Ml100 values, the peaks are culled by distance. The process starts by stepping from *MinCutoffDist* to *MaxCutoffDist*, using the step size *StepCutoffDist* and finding any peaks that are below the distance. Outliers are removed using the *OutlierThreshold* and [Chauvenaut's rejection theorem](https://en.wikipedia.org/wiki/Chauvenet%27s_criterion). Once enough peaks, as defined by *MinChannels* are found, then the stepping over cutoff distances is stopped, and the Preliminary Ml is calculated from the remaining peaks.  

**PrelimMlMagalgo** *string* *trimag*   
This is OPTIONAL and can be used to set the netmag.magalgo field in the db for this computation if this Ml is used (DEFAULTS to trimag).  

**RequireBothHorizontals** *number*  *0*  
This is a boolean OPTION (set to 1 to be on) to require both horizontals for a station to have valid readings before they are used. Also used for Final Ml computation. (DEFAULTS to off).  

**MinCutoffDist** *number*  
The minimum cutoff distance (km) for prelim Ml calculations.

**MaxCutoffDist** *number*  
The maximum cutoff distance (km) for prelim Ml calculations.  

**StepCutoffDist** *number*  
The step value for incrementing the test cutoff distance during the preliminary cutoff distance calculation.

**DefaultCutoffDist** *number*  
The default value for the final cutoff distance when a preliminary Ml magnitude cannot be determined.

**MinFinalMlCutoffDist** *number*  
Include channels for final ML less than minimum distance in km

**MinChannels** *number* *10*  
The minimum number of channels need to fall within a cut-off distance from the event hypocenter before that distance is accepted as the preliminary cut-off distance. (DEFAULTS to 10). (This also is used in the Final ML Computation for the Minimum number of good channels to use in the computation).


**ADA selection parameters**  

The below values are used to select the amplitude packets that should not be rejected. In addition, any amplitude packets that have gaps or flags set from the rad2 program will reject the channel from being used in the channel magnitude computation. The SNRcutoff parameter can strongly govern the computation of lower magnitude earthquakes when high amplitude noise on distant stations pass this critieria and influence the magnitude to larger numbers.

**MinBBValue** *number*  
The minimum value that an Broad Band (Velocity Sensor) channel which the peak value in the ADA must be above. This parameter must be specified in cm/sec units.  

**MaxBBValue** *number*   
The maximum value that an Broad Band (Velocity Sensor) channel which the peak value in the ADA must be below. This parameter must be specified in cm/sec units.

**MinSMValue** *number*  
The minimum value that an Strong Motion (Acceleration Sensor) channel which the peak value in the ADA must be above. If this is set too low, there will be many extra Strong Motion channels introduced into the magnitude. This parameter must be specified in cm/sec2 units.

**MaxSMValue** *number*  
The maximum value that an Strong Motion channel (FBA Acceleration) which the peak value in the ADA must be below. This parameter must be specified in cm/sec2 units.

**SNRcutoff** *number*  
The signal to noise ratio value that all ADA peak values must be above. The SNR is calculated in the new Trimag as the raw peak of the ml100 window over the LTA of the first ADA window (LTA defined above in rad).

**OnlyHorizontals** *number*  
Boolean. If set to 1, then only the Horizontal channels will be used in the magnitude solution.

**ArchiveDirectory** *string*  
If present, then this directory is where the event id is persistently stored till the event is completely processed by trimag. Once the event magnitude has been written to the database, then the evid is removed and processing has been completed.


**Windowing parameters for peak value searches**  

**VelocityModel** *string*  
Specifies the filename which contains the 1d layered model for P and S wave travel time calculations. The newest trimag uses the tlay.c routines from Earthworm. The details of the Velocity Model file are specified below.

**UseCITWindows** *number*  *1*  
Boolean. If set to 1 (ON by default), then the CIT windowing scheme will be used (described in the codes below), otherwise the postBuf, postSMult, preBuf settings are used.
```
win.start = or.origdate;
win.start += Duration(ptime);
win.start -= Duration(prebuf);
win.end = win.start;
win.end += Duration((S_MINUS_P_MULTIPLIER * (stime - ptime)) + PRELIM_WIN_TD);
```
where S_MINUS_P_MULTIPLIER is set to 1.8 in the CIT model and PRELIM_WIN_TD is fixed at 20 seconds. This windowing model was calibrated for California. The alternative is below:

**PreBuffer** *number*  
The of number of seconds prior to the estimated P arrival to use as the window start time for peak amplitude calculations. Start of channel's amp scan window = (predicted_P_time - PreBuffer) seconds for all Ml and Me implemented methods

**PostBuffer** *number*  
The number of seconds to search after the S.  

**PostSMultiplier** *number*  
The multiplier to multiply by the S - P time. See the description below:
```
win.start = or.origdate;
win.start += Duration(ptime);
win.start -= Duration(prebuf);
win.end = or.origdate;
win.end += Duration(stime);
win.end += Duration((postSMult * (stime - ptime)) + postbuf);
```

**Final Ml computation**  

**Note**: The prelim ML is used to compute a cutoff distance using this formula:  
```
cutoff in km = (170 * PrelimMl?) - 205 or 30 km Min and 600 km Max  
```  

**FinalMlMagalgo** *string*  *trimag*  
OPTIONAL. It can be used to set the netmag.magalgo field in the db for this computation if this Ml is used (DEFAULTS to trimag).  

**MinMlChannels** *number*  *5*  
This is the minimum number of channels needed for a final Ml calculation! (DEFAULTS to 5).  

**MinStations** *number*  *2*  
The minimum number of stations needed in the Final Ml and Final Me calculations (in addition to min channels). (DEFAULTS to 2)  

**OutlierThreshold** *number*  
The threshold value for Chauvenet's outlier rejection test.

**ewLogA0** *string*  
OPTIONAL. The absolute path to an Earthworm localmag Richter LogA0 relation file. See the ew docs on this: http://www.earthwormcentral.org/documentation4/cmd/localmag_cmd.html . If this is not set, then the CISN LogA0 relation will be used. This is only used for the ML100 computation by Rad.  

**MlClipValue** *number*  
The upper magnitude limit for an Ml to be considered preferred over an Me.  

**Ml adjustment**. This is for OPTIONAL adjusting the Ml (both preliminary and final) in case it deviates significantly from the newer Mw (moment) magnitude.  

**aValueMladjust** *number*  
The A value in MLadjusted = A + B * ML  

**bValueMladjust** *number**  
The B value in MLadjusted = A + B * ML  

**minMladjust** *number*  
The minimum Ml value that an Ml must be above to be adjusted (inclusive).

**maxMladjust** *number*    
The maximum Ml value that an Ml must be below to be adjusted (inclusive).


**Me computation**  

**MeMagalgo** *string*  *trimag*  
OPTIONAL. It can be used to set the netmag.magalgo field in the db for the Me magnitude (DEFAULTS to trimag).  

**MinMeChannels** *string*  
The minimum number of on scale peak amplitudes that must be present for an Me magnitude to be preferred over an Ml.  

**MaxMeRMS** *string*  
The maximum Me RMS allowed in order for the Me value to be preferred over an Ml.  


**CMS Signals**  

**EventSubject** *string* */signals/eventcoord/event*  
The CMS signal to receive an event id (evid) on.

**MagnitudeSubject** *string* */signals/trimag/fast_magnitude*  
The CMS signal to publish the evid to once a magnitude is written to the database. Used by alarmdec and tc.


**Database connection and storage**  

**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.  

**DBPasswd** *string*  
The password for the database user account specified by DBUser.  


*A sample configuration is shown below*

```
#
# TriNet Mag Configuration File
#

#
# General configuration parameters
#
CMSConfig       ../cms/conf/cms.cfg
Include         ../cms/conf/common_signals.cfg
Logfile         ../rt/logs/trimag_fast
LockFile        ../rt/locks/trimag_fast.lock
ConsoleLog      0
LoggingLevel    1
ProgramName     TriMag2
HSInterval      2

# GCDA configuration items
ADAKeyName      ADA_KEY

# Magnitude configuration settings
PropagationDelay        70.0
MaxDataLatency          10.0
OutlierThreshold        0.5
MinCutoffDist           100.0
MaxCutoffDist           800.0
StepCutoffDist          100.0
DefaultCutoffDist       300.0
MinChannels             10
MinMlChannels           5
MinStations             2
SNRcutoff               6.0
MinBBValue              0.0001
MaxBBValue              10000000.0
MinSMValue              0.5
MaxSMValue              10000000.0
PreBuffer               1

VelocityModel           ../ew/run/params/eq/scal_model.d
ArchiveDirectory        ./events_fast

# Criteria which determines when an Me is preferred over an Ml magnitude

MlClipValue             6.5
MinMeChannels           10
MaxMeRMS                1.0

# A test of the ML adjustment option
aValueMladjust 0.44731
bValueMladjust 0.86804
minMladjust 4.95
maxMladjust 5.99

EventSubject            /signals/eventcoord/event
MagnitudeSubject        /signals/trimag/fast_magnitude

#
# Database configuration settings
#
Include ../db/db_info.d
```
**Velocity model file**  

The velocity model format is the same used in Earthworm. It is specified by providing the depth of the top of a layer and the P wave velocity for the layer. S wave velocities are computed from a P to S ratio which can also be specified. An sample file is provided below.  Note that comments are defined by a # char at the beginning of the line.
```
# socal 1-d model for trimag
 
psRatio 1.73
 
#       depth   Vp
layer   0.0     5.5
layer   5.5     6.3
layer   16.0    6.7
layer   32.0    7.8 
```

**Station metadata needed for trimag**

As alluded to in the description of the module, the simple_response table in the database must be populated in order for a stations channels to be used. Trimag has been written to only operate on Broadband and Strong motion sensors at this time and the gain and gain_units field of the simple_response table must be populated in order for the station to be used in a Me or Ml computation.

Likewise, the stacorrection table must be populated with mecorr and mlcorr type station corrections. mecorr set to 1 in the table implies no station correction and mlcorr set to 0 implies no station correction for a given channnel. The Ml correction is ADDED to the uncorrected station magnitude value. The logarithm of amplitude has already been applied to get this station magnitude. The Me correction is a MULTIPLIER for the energy value. The logarithm will applied to this corrected energy value.

## ENVIRONMENT

   If this program can be influenced by environment variables, describe them here.

## DEPENDENCIES

The trimag module depends on the ADA being created and populated by rad2. You can use the accmon gcda utility to see that the ADA is being populated and current.

To get its channels, trimag relies on the Oracle Database tables named simple_response (for gain values) and channel_data for sample rate. It also finds its list of channels to operate on from the program and config_channel tables.

This RT module uses CMS to communicate with downstream modules like alarming and the trigger coordinator (tc module).


## MAINTENANCE

Look at the log files and use the sqlplus to check on a particular set of amps used for magnitudes in the Oracle database. Trimag writes to the netmag, assocamo, amp, and event database tables. site specific details.

## BUG REPORTING

Report issues via https://gitlab.com/aqms-swg/aqms-rapid-magnitude/issues

## MORE INFORMATION

[rad2](https://gitlab.com/aqms-swg/aqms-rapid-amplitude-data), [gcda utils](https://gitlab.com/aqms-swg/aqms-gcda), and [conlog](https://gitlab.com/aqms-swg/aqms-tools).

ML and Me are computed using a recursive time domain filter approximation developed by Hiroo Kanamori and others described in "Continuous Monitoring of Ground-Motion Parameters", BSSA, V89, pp. 311-316, Feb. 1999. Filter coefficients depend on sampling rate and amps scale by simple response gain. Note that rad2 uses a Wood-Anderson seismometer gain of 2080 from R.A. Uhrhammer and E.R. Collins ("Synthesis of Wood-Anderson Seismograms from Broadband Digital Records, BSSA, V80, pp. 702-716, June 1990) rather than the nominal "standard" gain of 2800 used in the Kanamori and others (1999) paper.
