/** @file
 * @ingroup group_rapid_magnitude
 * @brief Header file for DatabaseTrimag.C 
 */
/***********************************************************/
/*
File Name :
        DatabaseTrimag.h

Original Author:
        Patrick Small

Description:


Creation Date:
        07 June 2000

Modification History:

        25 August 2014 AWW - Added private member:
                               double _nextEventSecs
                             public function:
                               GetNextOrigin(double nextEventSecs, Event &e)

    14 Jun 2016 - AWW added public function:
                              HasPrecedingEvent(double precedingEventSecs, Event &e, int *count)
                              returns count of events precedingEventSecs before datetime of input event id

	 08 Dec 2016 - 'unsigned long' parameters changed to 'uint32' as part of 64-bit AQMS upgrade
	 
Usage Notes:
*/
/**********************************************************/

#ifndef database_trimag_H
#define database_trimag_H

// Various include files
#include <stdint.h>
#include "Event.h"
#include "Database.h"

/*!
  Class for rapid magnitude calculator specific database tasks.
 */
class DatabaseTrimag : public Database
{
 private:

    int _GetArrivals(uint32_t orid, ChanArrivalList &al);
    int _GetOrigin(uint32_t orid, Origin &org);
    int _GetMagnitude(uint32_t magid, Magnitude &mag);
    int _WriteMagnitudes(Event &e, const char *network, const char *source);
    int _WriteAmplitudes(Event &e, const char *network, const char *source);
    
 protected:

 public:
    DatabaseTrimag();
    DatabaseTrimag(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseTrimag();

    /// Retrieve event information from event table
    int GetEvent(Event &e); 

    /// Write amplitudes and magnitudes to database
    int WriteMagnitudes(Event &e, const char *network, const char *source);

    /// Get earliest origin (org2) for any other event following within _nextEventSecs interval seconds of this event
    int GetNextOrigin(double nextEventSecs, double maxOrgDistKm, Event &e);

    /// returns count of events precedingEventSecs before this event
    int HasPrecedingEvent(double precedingEventSecs, Event &e, int *count);
};


#endif
