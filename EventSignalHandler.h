/** @file
 * @ingroup group_rapid_magnitude
 * @brief Header file for EventSignalHandler.C
 */
/***********************************************************/
/*
File Name :
        EventSignalHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_EVENT_SIG messages.


Creation Date:
        08 June 2000

Modification History:


Usage Notes:
*/
/**********************************************************/

#ifndef event_sig_handler_H
#define event_sig_handler_H


// Various include files
#include "Connection.h"


// Function prototype for event signal message handler
void handleEventMsg(Message &m, void *arg);


#endif
