/** @file
 * @ingroup group_rapid_magnitude
 * @brief Header file for Peak.C 
 */
/***********************************************************

File Name :
        Peak.h

Original Author:
        Patrick Small

Description:


Creation Date:
        07 June 2000


Modification History:
	23 Dec 2007 - leap second changes


Usage Notes:


**********************************************************/

#ifndef peak_H
#define peak_H

// Various include files
#include <map>
#include "Channel.h"
#include "TimeStamp.h"
#include "Duration.h"
#include "AmplitudeADA.h"


class Peak;

// Definition of a list of peak values
typedef std::multimap<double, Peak, std::less<double>, 
		std::allocator<std::pair<const double, Peak> > > PeakList;


/// Class representing peak amplitudes
class Peak
{
 private:

 public:
    Channel chan;
    double mag_data_point;
    TimeStamp mag_time;
    struct amplitude_data_type dp;
    TimeStamp winstart;
    Duration duration;
    int glitch;
    int outlier;
    int validcorr;	/* used to indicate both valid ml and me corrections */
    double snr_lta;	/* newly added for saving SNR computed in trimag */
    float corr_used;    /* the actual corr value used */

    Peak(); 
    Peak(const Peak &p); 
    ~Peak(); 

    Peak& operator=(const Peak &p); 
    friend ostream& operator<<(ostream &os, const Peak &p); 
};

#endif
